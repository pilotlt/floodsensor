'''Version 2.0D for ESP8266'''

import wifimgr
import machine
import utime
import urequests

pin = machine.Pin(2, machine.Pin.OUT)
pin.value(1)  #led is off

wlan = wifimgr.get_connection()  # connection to WLAN

if wlan is None:  # In case no connection - several blinks
    for i in range(3):
        pin.value(1)
        utime.sleep(0.5)
        pin.value(0)
        utime.sleep(0.5)
    while True:
        pass

while True:  # Send notifications every 30 sec until battery die
    
    url = 'http://192.168.8.115:8080/json.htm?type=command&param=udevice&idx=1&nvalue=5&svalue=1'
    r = urequests.get(url)
    pin.value(0)  # led on after notification sent
    utime.sleep(1)
    pin.value(1)
    utime.sleep(30)